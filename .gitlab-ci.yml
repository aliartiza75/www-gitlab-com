# NOTE: A combination of YAML anchors and the `extends` keyword are used to remove duplication:
# * `extends` can be used for composing mappings of different keys, but not composing different
#    values into the same array.
# * YAML anchors are to compose/override entries into the same array. For example,
#   * Multiple `rules` entries, which uses `- <<: *anchor` to introduce a reusable mapping
#     entry like `if:` as an array entry.
#   * Composing a reusable string command as a step of a `script:` entry, which uses `- *anchor` to
#     introduce a string as an array entry.
#   * Composing a reusable array of commands, which uses `- *anchor` to introduce a sub-array
#     as an array entry.  Note that this works because up to one level of nested arrays will
#     still be successfully interpreted as sequential commands in a Gitlab CI `script` entry.
#   * TIP: Use https://yaml-online-parser.appspot.com/ to experiment and test out your YAML
#   * TIP: Use https://gitlab.com/gitlab-com/www-gitlab-com/-/ci/lint to validate your YAML
# * Also note that not everything is de-duplicated or DRYed up. For example, direct invocations
#   of a script with no arguments, such as `script/deploy`, are simple enough that they are
#   listed directly. Extracting them to a YAML anchor would actually add more total characters
#   and unnecessarily increase complexity for no real benefit.
#
#
# Job key consistent order (keys which exist only in `default:` are not listed):
#   extends
#   image
#   services
#   interruptible
#   timeout
#   tags
#   stage
#   trigger
#   rules
#   needs/dependencies
#   variables
#   environment
#   cache
#   artifacts
#   parallel
#   before_script
#   script
#   after_script

# GENERAL/DEFAULT CONFIG:

stages:
  - prepare
  - build
  - test
  - deploy

default:
  # Note that the rspec job below uses a different image that also
  # includes chromedriver. If we update the Ruby version for this image,
  # we should also update it for the rspec job.
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:www-gitlab-com-2.6
  tags:
    - gitlab-org
  interruptible: true # All jobs are interruptible by default
  retry:
    max: 2 # This is confusing but this means "3 runs at max".
    when:
      - unknown_failure
      - api_failure
      - runner_system_failure
      - job_execution_timeout
      - stuck_or_timeout_failure

variables:
  ### PERFORMANCE ###
  # GIT_* variables to speed up repo cloning/fetching
  GIT_DEPTH: "10"
  GIT_STRATEGY: "fetch"
  GIT_SUBMODULE_STRATEGY: "none"
  # Disabling LFS speeds up jobs, because runners don't have to perform the LFS steps during repo clone/fetch
  GIT_LFS_SKIP_SMUDGE: "1"
  # NO_CONTRACTS speeds up middleman builds
  NO_CONTRACTS: "true"

  ### RELIABILITY ###
  # Reduce flaky builds via https://docs.gitlab.com/ee/ci/yaml/#job-stages-attempts variables
  GET_SOURCES_ATTEMPTS: 3
  ARTIFACT_DOWNLOAD_ATTEMPTS: 3
  RESTORE_CACHE_ATTEMPTS: 3
  EXECUTOR_JOB_SECTION_ATTEMPTS: 3

# RULES REUSED VIA YAML ANCHORS:

.if-master: &if-master
  if: '$CI_COMMIT_REF_NAME == "master"'

.if-master-www: &if-master-www
  if: '$CI_COMMIT_REF_NAME == "master" && $CI_PROJECT_ID == "7764"'

.if-merge-request: &if-merge-request
  if: '$CI_MERGE_REQUEST_ID'

.if-schedule-deploy-cleanup-old-deleted-files-master-www: &if-schedule-deploy-cleanup-old-deleted-files-master-www
  if: '$CI_PIPELINE_SOURCE == "schedule" && $RUN_SCHEDULED_DEPLOY_CLEANUP_OLD_DELETED_FILES == "true" && $CI_COMMIT_REF_NAME == "master" && $CI_PROJECT_ID == "7764"'

# COMMON JOBS REUSED VIA `extends`:

.ruby-cache:
  cache:
    key: "web_ruby-2.6-stretch"
    policy: pull
    paths:
      - vendor

.bundle-install:
  extends: .ruby-cache
  before_script:
    - bundle install --quiet --jobs 4 --path vendor

# PREPARE STAGE JOBS:

ruby-push-cache:
  extends: .bundle-install
  stage: prepare
  rules:
    - <<: *if-merge-request
    - <<: *if-master
  cache:
    policy: pull-push
  script:
    - echo "Pushing updated ruby cache..."

dont-interrupt-me:
  image: alpine:edge
  interruptible: false
  stage: prepare
  rules:
    - <<: *if-master
      allow_failure: true
    - when: manual
      allow_failure: true
  variables:
    GIT_STRATEGY: none
  script:
    - echo "# This job makes sure this pipeline won't be interrupted on master. It can also be triggered manually to prevent a pipeline from being interrupted. See https://docs.gitlab.com/ee/ci/yaml/#interruptible."

# This is a manual job for debugging any unexpected behavior encountered while refactoring the CI config
expose-ci-rules-variables:
  image: alpine:edge
  stage: prepare
  rules:
    - when: manual
      allow_failure: true
  variables:
    GIT_STRATEGY: none
  script:
    - echo "CI_COMMIT_REF_NAME = ${CI_COMMIT_REF_NAME}"
    - echo "CI_PROJECT_ID = ${CI_PROJECT_ID}"
    - echo "CI_MERGE_REQUEST_ID = ${CI_MERGE_REQUEST_ID}"
    - echo "CI_MERGE_REQUEST_TITLE = ${CI_MERGE_REQUEST_TITLE}"
    - echo "CI_MERGE_REQUEST_PROJECT_PATH = ${CI_MERGE_REQUEST_PROJECT_PATH}"
    - echo "CI_COMMIT_REF_SLUG = ${CI_COMMIT_REF_SLUG}"
    - echo "CI_PIPELINE_SOURCE = ${CI_PIPELINE_SOURCE}"
    - echo "RUN_SCHEDULED_DEPLOY_CLEANUP_OLD_DELETED_FILES = ${RUN_SCHEDULED_DEPLOY_CLEANUP_OLD_DELETED_FILES}"

# SCHEDULED OR TRIGGERED CONTENT-GENERATION JOBS:

generate-handbook-changelog:
  extends: .bundle-install
  timeout: 3h
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CHANGELOG_MD == "true"'
  needs: []
  script:
    - bundle exec bin/generate_handbook_changelog

generate-handbook-changelog-rss:
  extends: .bundle-install
  timeout: 3h
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CHANGELOG_RSS == "true"'
  needs: []
  script:
    - bundle exec bin/generate_handbook_changelog_rss

release-post-build:
  extends: .bundle-install
  timeout: 3h
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $RELEASE_POST_BUILD == "true"'
  needs: []
  script:
    - bundle exec bin/release-post-build

generate-release-post-items:
  extends: .bundle-install
  timeout: 1h
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $RELEASE_POST_ITEM == "true"'
  needs: []
  script:
    - bundle exec bin/release-post-item --no-local

# Update https://gitlab.com/gitlab-org/gitlab/-/releases
# and https://gitlab.com/gitlab-org/gitlab-foss/-/releases
# when release posts are published
update-gitlab-project-releases-page:
  extends: .bundle-install
  stage: build
  rules:
    - <<: *if-master-www
      changes:
        - source/releases/posts/*-released.html.md
      allow_failure: true
  script:
    - bundle exec rake release:ee:update_project_releases_page
    - bundle exec rake release:foss:update_project_releases_page

# Trigger a build of https://gitlab.com/gitlab-com/teampage-map when the team changes
rebuild-map:
  stage: build
  trigger:
    project: gitlab-com/teampage-map
  rules:
    - <<: *if-master-www
      changes:
        - data/team.yml
      allow_failure: true


# SHARED BUILD AND DEPLOY LOGIC USED VIA `extends` AND YAML ANCHORS:

.build-base:
  extends: .bundle-install
  stage: build
  needs: []
  artifacts:
    expire_in: 7 days
    paths:
      - public/

.build-top-level:
  extends: .build-base
  parallel: 9
  script:
    - if [[ "$CI_NODE_INDEX" == "8" ]]; then bin/crop-team-pictures; fi
    - find source/images/team -type f ! -name '*-crop.jpg' -delete
    - bundle exec middleman build --bail
    - if [[ "$CI_NODE_INDEX" == "$CI_NODE_TOTAL" ]]; then bundle exec rake -t pdfs; fi

.review-environment:
  variables:
    DEPLOY_TYPE: review
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_COMMIT_REF_SLUG.about.gitlab-review.app
    on_stop: review-stop
    auto_stop_in: 30 days

.production-environment:
  variables:
    DEPLOY_TYPE: production
  environment:
    name: production
    url: https://about.gitlab.com

# HANDBOOK PARTIAL BUILD JOBS:

build-handbook:
  extends: .bundle-install
  stage: build
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_TITLE !~ /blog post/i'
    - <<: *if-master
    - <<: *if-schedule-deploy-cleanup-old-deleted-files-master-www
  needs: []
  artifacts:
    expire_in: 7 days
    paths:
      - public/
  script:
    - cd sites/handbook && bundle exec middleman build --bail && cd -

# PROXY RESOURCE BUILD JOBS (BRANCH/MR AND MASTER):

# Generators should be cached every 24 hours. We need to make sure the
# cache doesn't get blown away by build-branch jobs.
.build-proxy-resource-base:
  extends: .bundle-install
  stage: build
  needs: []
  cache:
    key: "build_proxy_resource_ruby-2.6-stretch"
    policy: pull-push
    paths:
      - tmp/cache
      - vendor
  artifacts:
    expire_in: 7 days
    paths:
      - public/
  script:
    - export INCLUDE_GENERATORS="true"
    - export CI_BUILD_PROXY_RESOURCE="true"
    - bundle exec middleman build --bail

build-proxy-resource-branch:
  extends: .build-proxy-resource-base
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_TITLE !~ /blog post/i'

build-proxy-resource-master:
  extends: .build-proxy-resource-base
  rules:
    - <<: *if-master
    - <<: *if-schedule-deploy-cleanup-old-deleted-files-master-www

# BRANCH/MR MAIN TOP-LEVEL BUILD JOBS:

build-branch:
  extends: .build-top-level
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_TITLE !~ /blog post/i'

build-branch-blog-only:
  extends: .build-base
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_TITLE =~ /blog post/i'
  parallel: 9
  script:
    - find source/images/team -type f ! -name '*-crop.jpg' -delete
    - cd sites/blog && bundle exec middleman build --bail && cd -

# MASTER MAIN TOP-LEVEL BUILD JOBS:

build-master:
  extends: .build-top-level
  rules:
    - <<: *if-master
    - <<: *if-schedule-deploy-cleanup-old-deleted-files-master-www

# TEST STAGE TEST/LINTER JOBS:

root-files-checker:
  image: debian:stable-slim
  stage: test
  rules:
    - <<: *if-merge-request
  needs: []
  script:
    - (diff -u FILES <(find . -maxdepth 1 -mindepth 1 | sort) && /bin/echo "No files/directories are added or removed")
      || ( /bin/echo "It looks like you've added files to the root directory. If this was intentional, please update FILES to allow this file. If this was not intentional, please remove the file from Git and try again."; exit 1 )

lint 0 2:
  extends: .bundle-install
  stage: test
  rules:
    - <<: *if-merge-request
  needs: []
  script:
    - bundle exec rake lint

lint 1 2:
  stage: test
  rules:
    - <<: *if-merge-request
  needs: []
  script:
    - yarn install
    - yarn run eslint

lint job-families:
  image: registry.gitlab.com/gitlab-data/data-image/data-image:latest
  stage: test
  rules:
    - <<: *if-merge-request
      changes:
        - "source/job-families/**/*"
  needs: []
  script:
    - cd source/job-families/
    - python check_job_families.py

lint release-post-items:
  stage: test
  rules:
    - <<: *if-merge-request
      changes:
        - "data/release_posts/unreleased/*"
        - "data/categories.yml"
        - "data/stages.yml"
  needs: []
  script:
    - gem install json_schemer
    - bin/validate-release-post-item

rubocop:
  extends: .bundle-install
  stage: test
  rules:
    - <<: *if-merge-request
      changes:
        - "*.rb"
        - "**/*.rb"
        - ".rubocop.yml"
  needs: []
  script:
    - bundle exec rubocop

# The integration specs are disabled for now due to flakiness, until we split the handbook out to its own pipeline and
# middleman instance in the monorepo. See https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/43991 for more details.
#rspec-integration:
#  extends: .bundle-install
#  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.6.5-git-2.22-chrome-74.0-node-12.x-yarn-1.21-docker-19.03.1
#  stage: test
#  rules:
#    - <<: *if-merge-request
#      changes:
#      - "source/frontend/**/*"
#      - "spec/**/*"
#      - "**/*.{js,json,rb,yml}"
#      - ".rspec"
#  needs: []
#  artifacts:
#    expire_in: 7 days
#    paths:
#      - tmp/capybara
#    when: on_failure
#  script:
#    - bundle exec rspec --tag @feature

rspec-unit:
  extends: .bundle-install
  stage: test
  rules:
    - <<: *if-merge-request
      changes:
        - "spec/**/*"
        - "**/*.{js,json,rb,yml}"
        - ".rspec"
  needs: []
  script:
    - bundle exec rspec --tag ~@feature

js-tests:
  stage: test
  rules:
    - <<: *if-merge-request
      changes:
        - "source/frontend/**/*"
        - "spec/**/*"
        - "**/*.{js,json,rb,yml}"
  needs: []
  script:
    - yarn install
    - yarn run test

check-links:
  image: coala/base
  stage: test
  rules:
    - <<: *if-merge-request
      when: manual
      allow_failure: true
  needs: []
  script:
    - git fetch --unshallow && git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*" && git fetch origin master
    - git diff --numstat origin/master..$CI_COMMIT_REF_NAME -- | awk '/(.+\.md)|(.+\.haml)/ { print $3 }' > new_files
    - coala --no-config --ci --bears InvalidLinkBear --settings follow_redirects=True --files="$(paste -s -d, new_files)"

include:
  - template: Code-Quality.gitlab-ci.yml

code_quality:
  tags: [gitlab-org-docker]
  stage: test
  rules:
    - <<: *if-merge-request
      when: manual
    - <<: *if-master
      when: manual
  needs: []
  dependencies: []
  artifacts:
    paths:
      - coffeelint.json
      - gl-code-quality-report.json

dependency_scanning:
  image: docker:stable
  services:
    - docker:stable-dind
  tags: [gitlab-org-docker]
  stage: test
  rules:
    - <<: *if-merge-request
      when: manual
      allow_failure: true
  needs: []
  dependencies: []
  variables:
    DOCKER_DRIVER: overlay2
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env DEP_SCAN_DISABLE_REMOTE_CHECKS="${DEP_SCAN_DISABLE_REMOTE_CHECKS:-false}"
        --volume "$PWD:/code"
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/dependency-scanning:$SP_VERSION" /code

check-edit-links:
  extends: .bundle-install
# NOTE: Because of the time it takes to download all artifacts, this job causes the MR builds to run longer, so for now
#       we are only checking links in the handbook instead of all review dependencies. Since the logic for
#       the edit links is in the common footer, this should provide adequate coverage for major breakages or regressions in the edit
#       links logic.  Note that also allows us to use `needs` instead of `dependencies` because we are now under the current
#       10-dependency-max limit for `needs`. This also avoids the longer runs, because `needs` allows it to be put in the `test`
#       group and start immediately when the `build-handbook` job finishes. This is because `needs` doesn't block on all jobs in
#       the prior group finishing like `dependencies` does.
  needs:
    - build-handbook
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_TITLE !~ /blog post/i'
  script:
    - scripts/check-edit-links.rb

# APPLY REDIRECT JOBS (ONLY APPLIES TO STAGING AND PROD):

apply-redirects-staging:
  extends: .bundle-install
  stage: deploy
  rules:
    - <<: *if-merge-request
      changes:
        - data/redirects.yml
      when: manual
      allow_failure: true
    - <<: *if-master
      changes:
        - data/redirects.yml
      when: manual
      allow_failure: true
  environment:
    name: staging
  script:
    - export FASTLY_SRV_ID=$FASTLY_SRV_ID_STG
    - export FASTLY_SRV_VER=$FASTLY_SRV_VER_STG
    - export FASTLY_DICT_ID=$FASTLY_DICT_ID_STG
    - export FASTLY_EXACT_ERR_SNIPPET_ID=$FASTLY_EXACT_ERR_SNIPPET_ID_STG
    - export FASTLY_EXACT_RECV_SNIPPET_ID=$FASTLY_EXACT_RECV_SNIPPET_ID_STG
    - export FASTLY_LITERAL_ERR_SNIPPET_ID=$FASTLY_LITERAL_ERR_SNIPPET_ID_STG
    - export FASTLY_LITERAL_RECV_SNIPPET_ID=$FASTLY_LITERAL_RECV_SNIPPET_ID_STG
    - export FASTLY_REGEX_ERR_SNIPPET_ID=$FASTLY_REGEX_ERR_SNIPPET_ID_STG
    - export FASTLY_REGEX_RECV_SNIPPET_ID=$FASTLY_REGEX_RECV_SNIPPET_ID_STG
    - export FASTLY_API_KEY=$FASTLY_API_KEY_STG
    - bundle exec bin/apply-exact-match-redirects
    - bundle exec bin/apply-regex-redirects

apply-redirects:
  extends: .bundle-install
  stage: deploy
  rules:
    - <<: *if-master
      changes:
        - data/redirects.yml
  environment:
    name: production
  script:
    - export FASTLY_SRV_ID=$FASTLY_SRV_ID_PROD
    - export FASTLY_SRV_VER=$FASTLY_SRV_VER_PROD
    - export FASTLY_DICT_ID=$FASTLY_DICT_ID_PROD
    - export FASTLY_EXACT_ERR_SNIPPET_ID=$FASTLY_EXACT_ERR_SNIPPET_ID_PROD
    - export FASTLY_EXACT_RECV_SNIPPET_ID=$FASTLY_EXACT_RECV_SNIPPET_ID_PROD
    - export FASTLY_LITERAL_ERR_SNIPPET_ID=$FASTLY_LITERAL_ERR_SNIPPET_ID_PROD
    - export FASTLY_LITERAL_RECV_SNIPPET_ID=$FASTLY_LITERAL_RECV_SNIPPET_ID_PROD
    - export FASTLY_REGEX_ERR_SNIPPET_ID=$FASTLY_REGEX_ERR_SNIPPET_ID_PROD
    - export FASTLY_REGEX_RECV_SNIPPET_ID=$FASTLY_REGEX_RECV_SNIPPET_ID_PROD
    - export FASTLY_API_KEY=$FASTLY_API_KEY_PROD
    - bundle exec bin/apply-exact-match-redirects
    - bundle exec bin/apply-regex-redirects

# REVIEW/MR DEPLOY JOBS:

# Upload artifacts from latest master build as a baseline to prepare or "prime" the bucket when deploying MR review apps,
# so that only the artifacts modified by the current build will need to be uploaded to the bucket by the 'review' job.
review-prep-bucket:
  extends: .review-environment
  # This job would better belong in the `prepare` or `deploy` stage, but that is not possible due to its dependencies and dependencies on it
  stage: build
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_PROJECT_PATH == "gitlab-com/www-gitlab-com" && $CI_MERGE_REQUEST_TITLE !~ /\[(REVIEW APP SKIP|SKIP REVIEW APP)\]/i'
      allow_failure: true
  needs: []
  script:
    - mkdir -p public
    - curl -S -s -f -L -o /tmp/build-master-1.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%201/9' || echo 'WARNING - unable to download latest artifacts from job build-master 1/9.'
    - curl -S -s -f -L -o /tmp/build-master-2.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%202/9' || echo 'WARNING - unable to download latest artifacts from job build-master 2/9.'
    - curl -S -s -f -L -o /tmp/build-master-3.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%203/9' || echo 'WARNING - unable to download latest artifacts from job build-master 3/9.'
    - curl -S -s -f -L -o /tmp/build-master-4.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%204/9' || echo 'WARNING - unable to download latest artifacts from job build-master 4/9.'
    - curl -S -s -f -L -o /tmp/build-master-5.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%205/9' || echo 'WARNING - unable to download latest artifacts from job build-master 5/9.'
    - curl -S -s -f -L -o /tmp/build-master-6.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%206/9' || echo 'WARNING - unable to download latest artifacts from job build-master 6/9.'
    - curl -S -s -f -L -o /tmp/build-master-7.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%207/9' || echo 'WARNING - unable to download latest artifacts from job build-master 7/9.'
    - curl -S -s -f -L -o /tmp/build-master-8.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%208/9' || echo 'WARNING - unable to download latest artifacts from job build-master 8/9.'
    - curl -S -s -f -L -o /tmp/build-master-9.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-master%209/9' || echo 'WARNING - unable to download latest artifacts from job build-master 9/9.'
    - curl -S -s -f -L -o /tmp/build-handbook.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-handbook' || echo 'WARNING - unable to download latest artifacts from job build-handbook.'
    - curl -S -s -f -L -o /tmp/build-proxy-resource-master.zip 'https://gitlab.com/gitlab-com/www-gitlab-com/-/jobs/artifacts/master/download?job=build-proxy-resource-master' || echo 'WARNING - unable to download latest artifacts from job build-proxy-resource-master.'
    # We will ignore errors due to failed downloads and not fail the job (even though it is allowed to fail) until the following bug is fixed:  https://gitlab.com/gitlab-org/gitlab/-/issues/216055
    - unzip -q -o /tmp/build-master-1.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 1/9.'
    - unzip -q -o /tmp/build-master-2.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 2/9.'
    - unzip -q -o /tmp/build-master-3.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 3/9.'
    - unzip -q -o /tmp/build-master-4.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 4/9.'
    - unzip -q -o /tmp/build-master-5.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 5/9.'
    - unzip -q -o /tmp/build-master-6.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 6/9.'
    - unzip -q -o /tmp/build-master-7.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 7/9.'
    - unzip -q -o /tmp/build-master-8.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 8/9.'
    - unzip -q -o /tmp/build-master-9.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-master 9/9.'
    - unzip -q -o /tmp/build-handbook.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-handbook.'
    - unzip -q -o /tmp/build-proxy-resource-master.zip -d . || echo 'WARNING - unable to unzip latest artifacts from job build-proxy-resource-master.'
    - scripts/review-replace-urls
    - scripts/deploy

review:
  extends: .review-environment
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_PROJECT_PATH == "gitlab-com/www-gitlab-com" && $CI_MERGE_REQUEST_TITLE !~ /\[(REVIEW APP SKIP|SKIP REVIEW APP)\]/i'
      allow_failure: true
  # needs: # NOTE: We are using `dependencies` instead of `needs` because there is currently a limit of 10 `needs`. See https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/7541
  dependencies:
    - build-branch
    - build-proxy-resource-branch
    - build-handbook
    - review-prep-bucket
    - build-branch-blog-only
  script:
    - scripts/review-replace-urls
    - scripts/deploy

review-deploy-cleanup-old-deleted-files:
  extends: .review-environment
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_PROJECT_PATH == "gitlab-com/www-gitlab-com" && $CI_MERGE_REQUEST_TITLE !~ /\[(REVIEW APP SKIP|SKIP REVIEW APP)\]/i'
      when: manual
      allow_failure: true
  # needs: # NOTE: We are using `dependencies` instead of `needs` because there is currently a limit of 10 `needs`. See https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/7541
  dependencies:
    - build-branch
    - build-proxy-resource-branch
    - build-handbook
  variables:
    DEPLOY_CLEANUP_OLD_DELETED_FILES: 'true'
  script:
    - scripts/review-replace-urls
    - scripts/deploy

review-stop:
  extends: .review-environment
  stage: deploy
  rules:
    - if: '$CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_PROJECT_PATH == "gitlab-com/www-gitlab-com" && $CI_MERGE_REQUEST_TITLE !~ /\[(REVIEW APP SKIP|SKIP REVIEW APP)\]/i'
      when: manual
      allow_failure: true
  variables:
    DEPLOY_DELETE_APP: 'true'
  environment:
    action: stop
  script:
    - scripts/deploy

# STAGING DEPLOY JOB (manual):

deploy-staging:
  extends: .deploy-base
  stage: deploy
  rules:
    - <<: *if-master-www
      when: manual
      allow_failure: true
  dependencies:
    - build-master
    - build-proxy-resource-master
    - build-handbook
  variables:
    DEPLOY_TYPE: staging
    # NOTE: For now, until we decide if we are even keeping the staging environment, there is no
    #       staging version of 'deploy-cleanup-old-deleted-files'.  We will just have the
    #       staging deploy handle it.  This makes it slower, so we have changed it to be
    #       run manually only (see https://gitlab.slack.com/archives/C017LD25V9S/p1595285130013600)
    DEPLOY_CLEANUP_OLD_DELETED_FILES: 'true'
  environment:
    name: staging
    url: https://about.staging.gitlab.com
  script:
    - scripts/deploy

# PROD DEPLOY JOBS:
# Note: Eventually, as part of https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8302, the `deploy`
# job will be eliminated, and only the `deploy-cleanup-old-deleted-files*` logic will need to remain.

.deploy-base:
  extends:
    - .production-environment
  stage: deploy
  script:
    - scripts/deploy

.build-only-dependencies:
  dependencies:
    - build-master
    - build-proxy-resource-master
    - build-handbook

.deploy-cleanup-old-deleted-files:
  extends:
    - .deploy-base
  variables:
    DEPLOY_CLEANUP_OLD_DELETED_FILES: 'true'

deploy:
  extends:
    - .deploy-base
    - .build-only-dependencies
  rules:
    - <<: *if-master-www

deploy-cleanup-old-deleted-files-manual:
  extends:
    - .deploy-cleanup-old-deleted-files
    - .build-only-dependencies
  rules:
    - <<: *if-master-www
      when: manual
      allow_failure: true

deploy-cleanup-old-deleted-files-scheduled:
  extends:
    - .deploy-cleanup-old-deleted-files
    - .build-only-dependencies
  rules:
    - <<: *if-schedule-deploy-cleanup-old-deleted-files-master-www
