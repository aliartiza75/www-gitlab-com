---
layout: job_family_page
title: "Growth Marketing"
---

## Intro

Growth Marketing focuses on expanding GitLab's organic discovery with data-driven decision making. As a Growth Marketer at GitLab, you'd probably describe yourself as inquisitive, creative, organized, and diplomatic. You have experience growing web traffic to increase revenue with search opitmization, conversion testing, and product management. You enjoy creating tools and processes to continually improve the results of a prospective customers journey, and sharing results and insights with stakeholders throughout the company.

## Responsibilities

- Align online marketing to enhance GitLab's brand with developers, IT ops practitioners, and IT leaders.
- Be an expert on our buyer persona audiences, speicifically the language they use to describe problems GitLab solves and the way they prefer to interact with marketing and sales teams.
- Be an expert on our customer lifecycle, and ways in which customer needs are evolving/how our product meets them.
- Work with product team to implement marketing strategies across trial/product experience to impact both customer conversion and future retention/upsell.
- Grow inbound demand through GitLab’s marketing site by optimizing the customer journey:
  - Improve conversion rate from search results to website visitor.
  - Improve conversion rate from website visitor to gated submit event (resources, webcasts, trials).
  - Improve conversion rate from gated submit event to activated trial.
  - Improve conversion from activated trial to purchase.
- Drive innovation and experimentation on the marketing site & trial experience to improve inbound demand creation & conversion.
- Examine and report data across multiple channels to monitor marketing site growth.

## Job Grade
- The Growth Marketing Associate, Search is a [grade #5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.
- The Growth Marketing Manager, Search is a [grade #6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.
- The Senior Growth Marketing Manager, Search is a [grade #7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.
- The Growth Marketing Manager, Conversion is a [grade #6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.
- The Senior Growth Marketing Manager, Conversion is a [grade #7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.
- The Manager, Growth Marketing is a [grade #8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

## Levels

#### Growth Marketing Associate, Search:

- Conduct keyword research and analysis and identify keyword opportunities.
- Coordinate with content and product marketing teams to identity, optimize, and promote content for targeted keywords or phrases in order to increase traffic to our website from organic, paid, and social marketing and sponsorships.
- Work with extended team to identify and resolve search marketing blockers.
- Assist with building and improving digital marketing campaigns with better word choice and in some cases writing and editing text to better meet user intent.
- Monitor market trends and competitors to establish marketing opportunities and recommend and implement actions to improve visibility of site.
- Evaluate and report data across multiple channels to monitor marketing site growth.

##### Requirements
- 1-3 years experience in a search marketing or content marketing role.
- Experience with Google Analytics, Google Search Console, and at least one keyword research tool.
- Exemplary writing skills.
- Intermediate knowledge of keyword research and the behaviors of search engines.
- Proven ability to work on multiple projects at a time.
- Basic experience with web conversion rate optimization to improve lead generation, sales pipeline, and revenue.
- Ability to use GitLab

#### Growth Marketing Manager, Search:
- Provide recommendations for technical SEO improvements to the site.
- Implement our SEO strategy together with our marketing team and digital marketing agency.
- Grow traffic to about.gitlab.com through organic search marketing programs.
- Work with marketing and sales ops to evaluate and implement adtech and martech.
- Work with our content team to improve quality scores of landing pages for paid and organic search.
- Work with our content team to ensure the content we create on the marketing site ranks for our priority search terms.
- Report on marketing site growth from SEO and paid search.
- Ensure web pages are structured and coded in a way to enable consistent and accurate tracking.
- Work across teams to collaborate on standardized marketing reporting and dashboards, and present performance to the marketing department as well as stakeholders throughout the company.
- Troubleshoot and formulate solutions to issues with SEO and site lead flow.
- Evaluate and report data across multiple channels to monitor marketing site growth.

##### Requirements
- 3+ years in Search Engine Marketing role
- 1-3 years of enterprise software marketing experience.
- 3+ years experience Google Analytics (or related tool), Google Search Console and experience with at least one SEO tool.
- Technical/industry experience focused on SEO online advertising to improve lead generation, sales pipeline, and revenue.
- Analytics and reporting experience


#### Senior Growth Marketing Manager, Search:

- Define our SEO strategy together with marketing team and digital marketing agency.
- Build end-to-send SEO programs with recommended keywords, content, and implementation plans and coordinate across teams on execution.
- Improve closed-loop reporting and analysis capabilities for site and SEO.
- Uses local development workflow to manage changes on about.gitlab.com at scale

##### Requirements
- 5+ years in Search Engine Marketing role
- 5+ years experience Google Analytics (or related tool), Google Console and expert-level SEO tool experience.
- 3+ years of enterprise software marketing experience.
- Technical/industry experience focused on SEO to improve lead generation, sales pipeline, and revenue.
- Advanced analytics and reporting experience including advanced knowledge of Excel, Google Sheets, Tableau, SQL, and/or similar.

#### Growth Marketing Manager, Conversion
- Create, execute, and implement testing process and roadmap to  improve key action conversion across about.gitlab.com.
- Write copy for conversion optimization tests.
- Assist in creation and launch of conversion tests and insure accurate results.
- Proven ability to work on multiple projects at a time.
- Experience using a variety of testing applications.
- Work across teams to reach goals and track performance of tests.

#### Requirements
- 3+ years in conversion optimization role.
- 1-3 years of enterprise software marketing experience.
- 3+ years experience Google Analytics (or related tool) and JavaScript testing platform.
- Basic working knowledge of CSS/HTML.
- Technical/industry experience focused on web conversion rate optimization to improve lead generation, sales pipeline, and revenue.
- Analytics and reporting experience.

#### Senior Growth Marketing Manager, Conversion
- Proven experience with CSS/HTML/JavaScript.
- Advanced understanding of multiple types of conversion testing.
- Uses local development workflow to manage changes on about.gitlab.com at scale.
- Technical/industry experience focused on web conversion optimizaiton to improve lead generation, sales pipeline, and revenue.

##### Requirements
- 5+ years in conversion optimization role.
- 5+ years of enterprise software marketing experience.
- 5+ years experience Google Analytics and multiple testing methods
- Proven experience working with CSS/HTML/JavaScript
- Advanced analytics and reporting experience with emphasis on dashboard creation.

## Manager, Growth Marketing

The Manager of Growth Marketing for GitLab will have a background and hands-on experience in all areas of marketing: SEO, SEM, Paid and Organic Social, ABM, Paid sponsorships, and Conversion Rate Optimization. They have experience managing marketing teams, coordinating work across teams, and working with marketing agencies. They should also be able to maintain a budget, use analytics tools, CRM, MAT, and other marketing tools, and have a data-driven approach to marketing. Experience in content marketing and web site maintenance are also helpful in this role.

### Responsibilities
- Manage, build, and lead an effective team by coaching and developing existing members and closing gaps where needed through hiring.
- Build and implement Growth Marketing strategy within organization to drive full funnel of customer acquisition, retention, and upsell.
- Define, manage, and implement our overall organic search strategy together with Growth Marketing team.
- Create strategies and implement tests for conversion rate optimization (CRO) projects.
- Lead effort to create standardized marketing reporting and dashboards, presenting performance to the marketing department as well as stakeholders throughout the company.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#management-group)

##### Requirements
- 10+ years in a Digital Marketing role
- 5+ years of enterprise software marketing experience.
- Experience managing a marketing team.
- In-depth industry experience and knowledge in organic search and marketing analytics.
- Robust technical marketing and advanced digital analytics skills

## Performance Indicators
- Organic search traffic growth
- Inbound Marketing Qualified Lead growth
- Gated form conversion rate optimization
- Marketing website analytics coverage

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters.
* Selected candidates will be invited to schedule an interview with the Manager, Growth Marketing.
* Candidates will then be invited to schedule two separate interviews with members of the Brand and Digital Design team.
* Candidates will be invited to schedule an interview with our Director of Brand and Digital Design
* Finally, candidates may be asked to interview with our CMO
* Successful candidates will subsequently be made an offer via video or phone call
