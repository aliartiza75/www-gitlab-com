---
layout: job_family_page
title: "Professional Services Project Coordinator"
---

To learn more, see the [Professional Services Engineer handbook](/handbook/customer-success/professional-services-engineering)

## Senior Professional Services Project Coordinator

### Job Grade

The Senior Professional Services Project Coordinator is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).  

## Responsibilities

* Collaborate with Professional Services leaders to provide reports focused on financial and resource management data to aid with revenue and budgetary goals.
* Provide analysis that supports the business objectives of the Services organization, in alignment with corporate vision
* Run weekly time reports to estimate revenue; identify any resource or revenue corrective actions that are required to meet/beat monthly target
* Run monthly/quarterly billing; work with accounting to record revenue.
* Develop analytical reports for all sources of services revenue, consistent with company standards, and management direction.
* Assist Services leadership team with resource management and proactive forecasting processes.
* Coordinate with Project Management Team and other groups within Sales / Operations to obtain relevant data about the project opportunity and staffing requirements
* Subject matter expert for services personnel on all processes related to services revenue: time entry, project management processes relate to operations, customer billing, and staff utilization.
* Skills Matrix: Management of PS / Vertical Resource Skill Matrix, ensuring all PS resources have an updated Skills Matrix.
* Continually provide suggestions and implement improvements to business and reporting processes that keep up with the changes in the organization.
* Support Professional Services staff to better understand revenue, hours, and target attainment goals
* Act as liaison between the Project Management Office, Finance department, and Services staff.
* Ensure timely and complete new PS employee onboarding with content tailored to the new hires’ positions

## Requirements

* Bachelor’s degree required, preferably in Finance, Accounting, Statistics, or Business Management
* 4-6 years industry experience in a Professional Services Operations or similar role for a high-tech product company
* 2+ years of PSA tools experience required (i.e. NetSuite Openair, FinancialForce)
* Previous experience in a data-driven, analytics-centric role
* Working understanding of Professional Services cost structures strongly preferred
* Strong Microsoft Excel skills including experience with pivot tables, analyzing and formatting data, use of filters, and creating charts and graphs
* Excellent communication and presentation skills
* Must have incredible attention to detail, be self-starting, and possess strong organizational skills
* You share our values, and work in accordance with those values.
* [Leadership at GitLab](/company/team/structure/#director-group)
* Ability to use GitLab

## Performance Indicators

* [CSAT](/handbook/support/support-ops/#customer-satisfaction-survey-csat ) >8.0/10 and [Project Margin](/handbook/customer-success/professional-services-engineering/#long-term-profitability-targets ) > 20% for assigned projects

## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

- Qualified candidates for the Professional Services Project Manager will receive a short questionnaire.
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team.
- Next, candidates will be invited to schedule a first interview with our Director of Customer Success.
- For the Professional Services Project Manager role, candidates will be invited to schedule an interview with a Customer Success peer and may be asked to participate in a demo of a live install of GitLab.
- For the Federal Professional Services Project Manager role, candidates will be invited to schedule interviews with members of the Customer Success team and our Federal Regional Sales Director.
- Candidates may be invited to schedule an interview with our VP of customer success.

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).
