---
layout: handbook-page-toc
title: "GitLab Offboarding FAQ"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Timing

**When will my offboarding issue be created?**

It will be created on your last day, the People Experience Associate assigned to your offboarding is responsible for confirming your log-off time with you.

**When will my access be deactivated?**

Your access will be deactivated on your last day, after the team member has logged off for the day. The People Experience Associate will communicate with the departing team member a day or two before their last day to confirm their final logoff time.

**When will I receive my final pay?**

This depends on your location. When you complete your Exit Interview, the People Operations Specialist will clarify that your final check (or invoice period) will be for the pay period of `X` and includes `X` days of pay.

**When will I receive (if applicable) commissions payment(s)?**

Any commissions earned will be paid (directly deposited) on date provided by `nonuspayroll@ gitlab.com` to the People Ops Specialist, who will relay this information to you during your Exit Interview or prior to it. If you have any questions on this payment, please contact Swetha Akshyap, Sales Commissions Manager at `skashyap@gitlab.com`.

**When do I submit my final reimbursable expenses?**

Please create your final expense report to Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner. We ask that you submit these at least one week before your departure date.

**How do I submit reimbursable expenses if I don’t have a GitLab access to Expensify?**

Finance provides an invoice template that can be emailed to your personal email. Return the completed form to `uspayroll@gitlab.com`

### Benefits

**When will my benefits coverage end?**

Typically, benefits coverage ends at the end of the month during which you leave.

*USA*

If you are a US-based team member, your benefits will cease on last day of the month of your departure. You are eligible for [Consolidated Omnibus Budget Reconciliation Act (“COBRA”)](https://www.dol.gov/sites/dolgov/files/ebsa/about-ebsa/our-activities/resource-center/faqs/cobra-continuation-health-coverage-consumer.pdf), the COBRA administrator has been notified and will send out the paperwork to your home address on file. More information on timelines and contact information for our COBRA administrator can be found in the [COBRA section](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#cobra).  You may also be eligible under the Patient Protection and Affordable Care Act (“PPACA”) for subsidized health care options via the marketplace. If you are interested it is important that you sign up with the market place well before the 15th of the month to have coverage for the following month. Under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), if you need a certificate of credible coverage please download it from your current carrier's online portal or request it from the Compensation & Benefits team by emailing `total-rewards@ gitlab.com`. If you move, please email `people-exp@ gitlab.com` so that we can keep your address up to date in BambooHR.

**How do I roll over my retirement account and funds?**

Contact our 401(k) administrative vendor directly to get more information about this process. More information can be found on the [401(k)](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#401k-plan) section and [401(k) Rollover](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#401k-rollover) section of the US Benefits handbook page.

**What about my FSA Account(s)?**

All information on FSA accounts is in the [Discovery Benefits](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#discovery-benefits) section of the US Benefits handbook page.

*Canada*

If you are a Canada-based team member, your benefits will cease on your last day of work.

There may be options to convert elements of your benefit plan to individual coverage, without medical questions. You can contact the insurance company (Canada Life), or our brokers (SC Insurance), and plan Admin App (Collage) noted below for more information about your options. These options are typically available for only a limited period of time after your coverage terminates (typically either 30 or 60 days).

Canada Life: call 800.957.9777 and additional information is found [here](https://www.greatwestlife.com/common/contact/phone-directory.html)
SC Insurance: email `info@scinsurance.ca` or call 416.259.1166.
Collage: email `benefits.admin@collage.c`o or call 800.651.9632.

If there are any issues that they cannot help with, please contact the Total Rewards team by emailing `total-rewards@gitlab.com`.

**How will my stock options be managed? What will happen to my stock options if I have been at GitLab less than a year at my date of departure?**


All information is on the [Stock Options](https://about.gitlab.com/handbook/stock-options/) handbook page, specifically the [Exercise Window after Termination](https://about.gitlab.com/handbook/stock-options/#exercise-window-after-termination) section.

### Other

**What if I move?**

If you move, please email `people-exp@gitlab.com` so that the People Experience team can keep your address up to date in BambooHR.

**Can I purchase my laptop from GitLab?**

IT Ops will contact you after your last day to determine if this is a possibility.
