---
layout: handbook-page-toc
title: Manager Challenge
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Manager Challenge page. We are rolling out a pilot version of a 4 Week Manager Challenge in September 2020, "The GitLab Manager Challenge Program." All details relating to the challenge can be found here on this page. The first iteration of the program will be a pilot to a subset of team members at GitLab. Once the pilot has concluded, we will roll out the program to the broader GitLab community!

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vST_3shd7g0Y6E46JaCdtpXKHfj6D8TAjF-fgZ4IiZ_1NETN2f8ROjBE6NtOpCSs0YXwWgYq-oHryO9/embed?start=true&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

**What** 

The Four Week Manager Challenge Program is a blended learning approach that incorporates self-paced daily challenges and live learning sessions to build foundational management skills. The program incorporates leadership assessments, interactive live learning sessions, and digital learning. 

**Why** 

The program is intended to build a set of baseline skills that complement our values to enable Managers to lead teams at GitLab. To date, there has not been a formal GitLab Manager training program and this will serve as our first iteration. 

**How will it help**

Learn the basic principles of what it means to be a manager using a whole person approach to leadership. The program was built to incorporate themes identified during interviews with team members and managers at GitLab. 

**What do I need to do**

Set aside time each day to participate in the challenge and live learning sessions. Complete the weekly self-evaluations and comment in the Slack channel during discussions.

[embed pilot welcome video here] 

## Pilot Program 

- 12 Daily Challenges
- 4 Live Learning Sessions
- 4 Weekly Self Evaluations and Reflections
- Certification Upon Completion

### Week 1

During week one we will discuss **Getting to Know your Team & Managing Underperformance**. Information covered in week one can be found on the [SOCIAL STYLES](/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/) and [Underperformance page](https://about.gitlab.com/handbook/underperformance/). 

| Day     | Daily Challenge | Length of Time   |  Format | Activity | Competency Alignment |
|---------|-----------------|------------------|---------|----------|----------------------|
| Day 1    | 4 Week Challenge Intro | 20 minutes | Self-paced Daily Challenge | TBA |  |
| Day 2    | Get to know your SOCIAL STYLE | 20 minutes | Self-paced Daily Challenge | TBA | * Collaboration (Getting to know individuals on a personal and professional level) <br> * Diversity, Inclusion, & Belonging (Building a safe community for your team members) |
| Day 3    | Get to know your team | 20 minutes | Self-paced Daily Challenge | TBA | * Collaboration (Getting to know individuals on a personal and professional level) <br> * Diversity, Inclusion, & Belonging (Building a safe community for your team members) |
| Day 4    | Attend Live Learning | 50 minutes | Live Learning | TBA | * Results (Measure results not hours) <br> * Efficiency (Be respectful of other's time <br> * Manager of One <br> * Handbook First <br> * Iteration  |
| Day 5    | Self Reflection and Weekly Evaluation | 20 minutes | Virtual Team Meeting | TBA | * Collaboration (Getting to know individuals on a personal and professional level) <br> * Diversity, Inclusion, & Belonging (Building a safe community for your team members) |

[embed week 1 slide deck here] 

### Week 2

During week two we will discuss **Trust and Coaching**. Information covered in week two can be found on the [Coaching page](/handbook/people-group/learning-and-development/career-development/coaching/). 

| Day     | Daily Challenge | Length of Time   |  Format | Activity | Competency Alignment |
|---------|-----------------|------------------|---------|----------|----------------------|
| Day 6   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 7   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 8   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 9   | Attend Live Learning | 50 minutes | Live Learning | TBA | TBA |
| Day 10  | Self Reflection and Weekly Evaluation | 20 minutes | Self-paced Daily Challenge | TBA | TBA |

[embed week 2 slide deck here] 

### Week 3

During week three we will discuss **Building and Inclusive/Belonging Environment**. Information covered in week three can be found on the [page](). 

| Day     | Daily Challenge | Length of Time   |  Format | Activity | Competency Alignment |
|---------|-----------------|------------------|---------|----------|----------------------|
| Day 11   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 12   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 13   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 14   | Attend Live Learning | 50 minutes | Live Learning | TBA | TBA |
| Day 15   | Self Reflection and Weekly Evaluation | 20 minutes | Self-paced Daily Challenge | TBA | TBA |

[embed week 3 slide deck here] 

### Week 4

During week four we will discuss **Developing High Performing Teams**. Information covered in week four can be found on the [Building High Performing Teams page](/handbook/people-group/learning-and-development/building-high-performing-teams/).  

| Day     | Daily Challenge | Length of Time   |  Format | Activity | Competency Alignment |
|---------|-----------------|------------------|---------|----------|----------------------|
| Day 16   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 17   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 18   | TBA | 20 minutes | Self-paced Daily Challenge | TBA | TBA |
| Day 19   | Attend Live Learning | 50 minutes | Live Learning | TBA | TBA |
| Day 20   | Self Reflection and Weekly Evaluation | 20 minutes | Self-paced Daily Challenge | TBA | TBA |

[embed week 4 slide deck here] 


## FAQs

1. **What is the Manager Challenge Program?**
     - The program is intended to build a set of baseline management skills through micro habits and daily practices over the course of 4 weeks. 
1. **Is this a Pilot launch?**
     - Yes. In September we are running a pilot and will be rolling out the program to the wider GitLab team very soon.
1. **Do I have to participate?**
     - Participation is not required, but we strongly recommend that all managers invited to the pilot complete the challenge. 
1. **What is required of me?**
     - Monday to Wednesday there will be a series of daily challenges that a manager can complete asynchronously on their own time that will cover a range of topics. On Thursday's we will come together as a group in a live learning session to apply the challenges to real scenarios managers face in a remote environment. 
1. **I have a lot of management experience, will this help?**
     - No matter your level or tenure as a manager, we can all take time out of our busy days to grow our management and leadership skills. The more practice the better and we hope that you will find the challenges applicable to your job.
1. **What skills will I be building?**
     - You will be building a range of skills that include: coaching, managing underperformance, crucial conversations, feedback, building an inclusive culture, developing high performing teams, and much more!  
1. **How does this complement our values?**
     - All of the challenges will reinforce our values by applying management techniques to lead others in a remote environment.  
1. **Is the content in the slides in the Handbook?**
     - Yes! All of the content will be in the Handbook. We are creating Google Slides to visualize the content for the challenges but all of it will live in the handbook. 
1. **What if I can’t attend a Live Learning session or complete a challenge?**
     - If you miss a challenge or live learning session you can go back and complete the challenge anytime. Each SSOT page for material covered will be linked on this page. The live learning recordings will be on the respective SSOT page (ex. the recording for the Week 3 Trust & Coaching session will be on the [Coaching page](/handbook/people-group/learning-and-development/career-development/coaching/)). 
1. **What if I am out of office for part of the program?**
     - As long as you complete the challenge and let the Learning and Development know when you have completed them, it is okay to be out of office. 
1. **I'm not a Manager, can I still participate?**
     - Yes, you can still participate but we ask that you complete the activities asynchronously for your own professional development. Once you are ready to become a manager, you will be equipped with a set of baseline management skills. 
1. **How do I receive a certification?**
     - Yes! Once you complete all challenges, attend the live learning sessions, and complete the self-reflection activities, you will receive the "GitLab Managers Challenge" certificate.
