---
layout: handbook-page-toc
title: "People Experience Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# People Experience Team

## People Experience Team Availability

Holidays with no availability for onboarding/offboarding/career mobility issues:

| Date                        |
|-----------------------------------|
| 2020-05-01 |
| 2020-12-25 |
| 2021-01-01 |

### OOO Handover Process for People Experience Team

1. The People Experience Associate will create a document the day before their scheduled OOO with the following:
- What urgent tasks are there that need to be reassigned:
* Onboarding
* Offboarding
* Career Mobilities
* Any other tasks
- Is there anything else that we should be made aware of and contribute to whilst you are away?
2. The People Experience Coordinator will then reassign tasks to an alternative People Experience Associate.
3. Get assistance from the People Operation Specialist team if additional hands are needed.

### OOO Process for People Experience Coordinator

1. When the People Experience Coordinator is due to be OOO, the People Experience Associates will allocate the relevant tasks between themselves.
2. In the event that there are items that need urgent attention whilst the People Experience Coordinator is OOO, the Coordinator will hand over the relevant tasks to the People Experience Team Lead or People Experience Associates.

## People Experience Team Processes

### Weekly Rotations 

The People Experience Team are currently trialling a task rotation on a weekly basis. The allocation tracker can be found in the [People Exp/Ops Tracker]. This will initially be trialed on a weekly rotation and then possibly moved to monthly. 

A task will be allocated based on a ongoing team member rotation between all People Experience Associates. 

The following factors will also be taken into consideration:

- Scheduled PTO for the team members
- Ensure that the tasks split evenly and fairly 
- A tracker will be kept to track the data of how the tasks are split up

The Rotation groups are listed as follows:

- Letters of Employment / VOE 
- Probation / Anniversary Queries / Gift Requests
- BHR Onboarding Report / Moo Report
- Allocations for Onboarding / Offboarding / Mobility
- I-9 Administration / Pruning of Stale Records

#### Allocations for Onboarding

- We always try and split evenly and fairly

#### Allocations for Offboarding

- Team has 12 hours to create and notify of the offboarding issue
- The person that is in the allocation rotation will also add the offboarding to the PEA team calendar
- Due to different time zones, the offboarding issue creation and task completion can be tagged team by the Associates. 

#### Allocations for Career Mobility 

- We always try and split evenly and fairly


### Audits 

There are certain tasks that need to be completed by all the different departments and team members and we as the People Experience Team need to ensure to remain compliant in line with these tasks. Herewith a breakdown of the important compliance and team tasks:  

- Onboarding

This will be the responsibility of the People Experience Associate that is assigned to the specific team members onboarding. 

    - Ensure that the new team member has shared a screenshot of their FileVault disk encryption and computer serial number in the onboarding issue. 
    - Ensure that the new team member has acknowledged the 'Handbook' compliance task in the onboarding issue. 
    - Ensure that the manager tasks are completed prior to and after the new team member has started. 
    - Ensure that an onboarding buddy has been assigned to assist the new team member. 
    - Ping the relevant team members to call for a task to be completed in the onboarding issue.

- Offboarding

This will be the responsibility of the People Experience Associate that is assigned to the specific team members offboarding. 

    - Ensure that all the different departments complete their tasks within the 5 day due date. 
    - Immediate action is required from the People Experience Team and the IT Ops Team once the issue has been created. 
    - Ping the relevant team members to call for a task to be completed in the offboarding issue.
    - Confirm that a departure announcement has been made in #team-member-updates on Slack.

- Career Mobility 

This will be the responsibility of the People Experience Associate that is assigned to the specific team members career mobility issue. 

    - Check to see whether the team member that has migrated needs any guidance.
    - Ensure that the previous manager and current manager completes their respective tasks. 
    - The issue should be closed within 2 weeks of creation, ping the relevant team members to call for a task to be completed in the issue.


**Over and above these audits, the Compliance Specialist will perform their own audits to ensure that certain tasks have been completed.**

### Pulling of BambooHR Onboarding Data

This WIP and will be included here soon. 

- Report is pulled every Monday and Wednesday

### OSAT Team Member Feedback

Once a new team member has completed their onboarding, they are asked to complete the `Onboarding Survey` to share their experience. Based on the scores received, the People Experience Associate assigned to the specific team members onboarding, will complete the following:

- Score of 4 or higher: use own discretion based on the feedback received and see whether there are any improvements or changes that can be made to the onboarding template / onboarding process (this can be subjective). 
- Score of 3 or lower: reach out to the team member that has provided feedback and schedule a coffee chat to further discuss their concerns and feedback provided. 

#### Onboarding Buddy Feedback

In the same survey, new team members are able to provide a score and feedback on their onboarding buddy. If the score and feedback received is constructive and valuable insights when the score is low, the People Experience Associate assigned to that specific team members onboarding, should reach out to the manager of the onboarding buddy and provide feedback in a polite and supportive way.  

### Anniversary Period Gift Queries

*Current Process*

Team members celebrating their first, third and fifth anniversaries with GitLab would have received an email from the People Experience Team asking them to complete their information in order to have their item shipped. Due to production and shipping delays, many team members are still waiting for their gifts and are instructed to make contact with people-exp@gitlab for follow up. Once we receive the request:

1. Advise the team member that we will follow up with the vendor and get back to them. 
1. Send an email to the DRI at the vendor and ask for feedback on the team members order, providing the team members name and anniversary unique code. Please cc People Experience Team Lead. 
1. Add the team members request to the `Anniversary Pending Feedback` spreadsheet in Google Drive. 
1. Once feedback is received, depending on the situation, we will still need to get approval that the item may be shipped or an order needs to be placed again. Important to note that there are time delays in getting the approval required in order to get back to the team member.  

### Gift Requests

When a team member completes the GitLab gift form request, the People Experience team receives an email to people-exp@gitlab.com to process the request. Most often, these are requests for flowers to be sent to another team member. Please see the below steps for guidance on how to process these requests:

- Navigate and open the gift form requests in `Google spreadsheet`. 
- Open the PeopleOps 1Password Vault and select `Gift & Flower Vendors` to gain access to the various vendors used. 
- If a voucher for meal delivery is requested, ensure to check that the vendor is available in the team members location. 
- Place order and once confirmed, add data, including order confirmation link, to spreadsheet. 
- Send the requesting team member an email or message in Slack to confirm that you have processed the request/order. 
- Use the Gift [page](https://about.gitlab.com/handbook/people-group/#gifts) in the Handbook for any further information regarding the policy for gift order requests. 


### Scheduling Group Conversations 

The People Experience team assists with the administration of the GitLab Team Calendar and automatically can assist with the following:

- Scheduling new Group Conversations or AMA's
- Assist with updating the calendar when Group Conversations are rescheduled

Here are the steps to follow when scheduling a Group Conversation or AMA:

1. Log into the PeopleOps Shared Zoom Account via Okta
1. Select 'Schedule a New Meeting'
1. Insert the name of the conversation under 'Topic'
1. Below the date and time option, select 'Recurring Meeting'
1. Under the 'Recurrence' dropdown, select 'No Fixed Time'
1. Select the 'Require meeting password' option. This will automatically generate a password for you. 
1. Ensure that the Video is on for both 'Host' and 'Participant'
1. At the bottom of the page, untick 'Enable waiting room' 
1. You are also able to add any alternative hosts in the event that the People Group will not be assisting with moderating the call. 
1. Click 'Save'
1. Once saved, select the option to 'Add to Google Calendar'
1. You will be redirected to the Google Calendar and prompted to select a calendar. Select your own account. 
1. Confirm allowing access for Zoom to Google Calendar.
1. You will now be able to edit the calendar invitation with the following:
    - Date and time (always needs to be scheduled in PST) and the meeting time should be set to 25 minutes. 
    - Add the description and agenda link 
    - Add the Host and Moderator to the description
    - Add to the GitLab Team Meetings
    - Invite everyone@gitlab.com 
1. Click 'Save' 
1. You will be prompted to select whether you would like to send invitation emails to Google Calendar guests? Select 'Don't Send'
1. You will receive a bunch of automated messages in your personal Inbox regarding the scheduled call, you can simply ignore and delete them. 

That's it, you have successfully scheduled the meeting! Now just remember to let the requesting team member know to check that you have completed the request. 


### HelloSign

As the DRI for HelloSign, when a team member needs to have access to sign documents for the company, an access request needs to be created and assigned to the People Experience Team for provisioning. 

#### Process once Access Request is received:

1. Log into [HelloSign](https://app.hellosign.com/home/) using your personal account information
1. Select `Team` on the left hand-side of the page
1. Insert the team members GitLab email address and click `Invite`
1. Take a screenshot of the confirmed invitation sent and upload to the Access Request as confirmation

#### Monthly Billing

1. When monthly expenses are due, we need to be able to provide Finance with the specific team members name in order to assign to the correct Department. To do this we would need to send a request to HelloSign Support on a monthly basis and often the response is not received in time for the monthly expenses. Once the list is received from HelloSign, simply send the Accounts Payable Specialist an email with the information.  


### Regeling Internet Thuis form

New team members based in the Netherlands will send an email to people-exp@gitlab.com with the Regeling Internet Thuis form. The People Experience team will then forward this form to the payroll provider in the Netherlands via email. The contact information can be found in the People Ops 1Password vault, under "Payroll Contacts"
